// Теоретичні питання
// Опишіть своїми словами що таке Document Object Model (DOM)
// Коли браузер загружає HTML-код, він будує на основі HTML-коду DOM - це деревоподібне
// відображення сайту, завантажуваємого в браузер. Це логічний каркас документу, за допомогою 
// DOM ми можемо створювати , додавати, видаляти елементи.  


// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML - при виводі innerHTML ми бачимо і контент, і HTML-розмітку, яка може попасти
// між відкриваючими та закриваючими тегами основного елементу.
// innerText - при виводі innerText ми побачимо весь текстовий контент без HTML-розмітки.
// При цьому якщо всередині будуть якісь HTML-теги,innerText проігнорує їх та поверне лише текст. 

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Звернутися до елемента можна декількома способами:
// 1. document.getElementById(id) - шукає елемент по id;
// 2. elem.querySelectorAll(css) - шукає всі елементи з відповідним класом в дужках; 
// 3. elem.querySelector(css) - шукає перший елемент з відповідним класом в дужках;
// 4. elem.getElementsBy(tag || className || name) - пошук можна зробити по тегу, назві класу, 
// по атрибуту name.
// Найбільш використовуваними методами є querySelector та querySelectorAll, але при необхідності
// можна користуватись методами getElement(s)By*. Не можна сказати, який спосіб кращий - все залежить від задачі.



// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let parag = document.getElementsByTagName('p');
for (let key of parag) {
    key.style.backgroundColor = "#ff0000";
};

// Знайти елемент із id="optionsList". Вивести у консоль. 
// Знайти батьківський елемент та вивести в консоль. 
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let element = document.getElementById('optionsList');
console.log(element);
console.log(element.parentElement);

if(!element.childNodes.length) {console.log ("Дочірніх нодів немає")}
else { 
    for(let node of element.childNodes) {
    console.log(`Тип ноди ${node.nodeType} Назва ноди ${node.nodeName}`)
}
}
 

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph

let testParagraph = document.getElementById('testParagraph');
console.log(testParagraph.innerText);
testParagraph.textContent = "This is a paragraph";
console.log(testParagraph.innerText);


// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let header = document.querySelector('.main-header');
let children = header.children;
for(let i = 0; i < children.length; i++){
    console.log(header.children[i]);
}

for(prop of header.children) {
    prop.classList.add('nav-item');
    console.log(prop);
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);

for(let key of sectionTitle){
    key.classList.remove('section-title');
    console.log(key);
}
